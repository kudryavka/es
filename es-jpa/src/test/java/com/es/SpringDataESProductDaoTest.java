package com.es;

import com.es.dao.ProductDao;
import com.es.entity.Product;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author ZH
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringDataESProductDaoTest {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;
    @Test
    public void search(){
        SearchHits<Product> search = elasticsearchRestTemplate.search((Query) QueryBuilders.matchQuery("title", "手机"), Product.class);
        List<SearchHit<Product>> searchHits = search.getSearchHits();
        for (SearchHit<Product> searchHit : searchHits) {
            System.out.println(searchHit.toString());
        }
    }

    @Test
    public void save(){
        Product product = new Product();
        product.setId(2L);
        product.setTitle("小米手机");
        productDao.save(product);
    }
}
