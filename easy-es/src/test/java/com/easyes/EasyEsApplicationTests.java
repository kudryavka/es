package com.easyes;

import com.easyes.entity.Document;
import com.easyes.mapper.DocumentMapper;
import com.xpc.easyes.core.conditions.LambdaEsIndexWrapper;
import com.xpc.easyes.core.conditions.LambdaEsQueryWrapper;
import com.xpc.easyes.core.conditions.LambdaEsUpdateWrapper;
import com.xpc.easyes.core.enums.FieldType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class EasyEsApplicationTests {
    @Resource
    DocumentMapper documentMapper;
    @Test
    public void tesLike() {
        //模糊查询 这里 只有like可以查询到  因为 hello才是一个词 match查不到
        String matchStr = "hell";
        List<Document> documents = documentMapper.selectList(new LambdaEsQueryWrapper<Document>().like(Document::getContent, matchStr));
        System.out.println(documents);
    }

    @Test
    public void tesMatch() {
        //分词查询  Document(id=SX91r38BbYgDx1D97x9h, title=李四, content=hello I like world)
        String matchStr = "hello world";
        List<Document> documents = documentMapper.selectList(new LambdaEsQueryWrapper<Document>().match(Document::getContent, matchStr));
        System.out.println(documents);
    }

    @Test
    public void testDelete() {
        // 测试删除数据 删除有两种情况:根据id删或根据条件删
        // 鉴于根据id删过于简单,这里仅演示根据条件删
        String title = "李四";
        int successCount = documentMapper.delete(new LambdaEsQueryWrapper<Document>().eq(Document::getTitle,title));
        System.out.println(successCount);
    }

    @Test
    public void testUpdate() {
        // 测试更新 更新有两种情况 分别演示如下:
        // case1: 已知id, 根据id更新 (为了演示方便,此id是从上一步查询中复制过来的,实际业务可以自行查询)
        Document document1 = new Document();
        document1.setContent("hello I like world");
        documentMapper.update(document1,new LambdaEsUpdateWrapper<Document>().eq(Document::getTitle,"李四"));
    }

    @Test
    public void testSelect() {
        // 测试查询
        String title = "李四";
        LambdaEsQueryWrapper<Document> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.eq(Document::getTitle,title);
        Document document = documentMapper.selectOne(wrapper);
        System.out.println(document);
    }

    @Test
    public void testInsert() {
        // 测试插入数据
        Document document = new Document();
        document.setTitle("李四");
        document.setContent("hello world lisi");
        documentMapper.insert(document);
        System.out.println(document.getId());
    }

    @Test
    void contextLoads() {
        // 测试创建索引 不了解Es索引概念的建议先去了解 懒汉可以简单理解为MySQL中的一张表
        LambdaEsIndexWrapper<Document> wrapper = new LambdaEsIndexWrapper<>();

        // 此处简单起见 直接使用类名作为索引名称 后面章节会教大家更如何灵活配置和使用索引
        wrapper.indexName(Document.class.getSimpleName().toLowerCase());

        // 此处将文章标题映射为keyword类型(不支持分词),文档内容映射为text类型(支持分词查询)
        wrapper.mapping(Document::getTitle, FieldType.KEYWORD)
                .mapping(Document::getContent, FieldType.TEXT);

        boolean isOk = documentMapper.createIndex(wrapper);
        // 期望值: true 如果是true 则证明索引已成功创建
    }

}
