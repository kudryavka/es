package com.easyes.mapper;

import com.easyes.entity.Document;
import com.xpc.easyes.core.conditions.interfaces.BaseEsMapper;

public interface DocumentMapper extends BaseEsMapper<Document> {
}