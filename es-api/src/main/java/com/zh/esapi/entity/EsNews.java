package com.zh.esapi.entity;

/**
 * @author ZH
 */
import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EsNews {

    private String id;

    private String title;

    private String cityCode;

    private String displayUrl;

    private String fetchTime;

    private long seqence;

    private String status;

    private String sourceTime;

    private String fromName;

    private String fromCode;

    private long sourceTimeIndex;

    private long sourceTimeDateIndex;

    private long fetchTimeIndex;

    private long fetchTimeDateIndex;

    private String host;

    private String mainHost;

    private String boardUrl;

    private String channelId;

    private String pageScore;

    private String region;

    private String picNum;

    private String contentType;

    private Long version;

    private String summary;

    private Integer readNums;

    private List<String> publishPlatFormId;

    private String mediaOrgId;

    private String mediaTypeId;

    private List<String> originImages;

    private List<String> imageNames;

    private String emotionValue;

    private List<String> newsClassification;

    private String content;

    private String contentText;

    private Map<String,Integer> readNum;

    private Map<String, String> originNewsUrl;


    private String isTop;

    private String topTime;

    private String channel;

    private Integer isOriginal;

    private Integer isContainSensitiveWord;

    private Integer isProcessedByAlgorithm;

}
