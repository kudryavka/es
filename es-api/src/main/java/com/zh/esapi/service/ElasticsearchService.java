package com.zh.esapi.service;


import com.zh.esapi.entity.EsNews;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author ZH
 */
public interface ElasticsearchService {

    List<Map<String ,Object>> searchSort(String title, String summary) throws IOException;

    /**
     * 搜索
     * @param title
     * @param summary
     * @return
     * @throws IOException
     */
    List<Map<String ,Object>> search(String title, String summary) throws IOException;

    String save(EsNews esNews) throws IOException;

    String deleted(String id) throws IOException;

    String update(EsNews esNews) throws IOException;

    EsNews getById(String id) throws IOException;

    List<EsNews> getByIds(List<String> idList)throws Exception;

    List<Map<String ,Object>> searchHighLight(String title, String summary) throws IOException;

    List<EsNews> searchAll() throws Exception;

    boolean updateBatchById(List<EsNews> list) throws IOException;

    /**
     * 根据 field 字段  聚合 key:字段值  value: count
     * @param field
     * @return
     * @throws IOException
     */
    Map<String,Object> aggsByField(String field) throws IOException;
}
